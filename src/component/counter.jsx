import React, { Component } from "react";

export default class Counter extends Component {
  state = {
    count: 2,
    imageUrl: "https://picsum.photos/200",
    tags: ["tag1", "tag2", "tag3"],
    product: { id: 23 },
  };

  styles = {
    fontSize: 30,
    fontWeight: "bold",
  };

  //   constructor() {
  //     super();
  //     this.handleIncrement = this.handleIncrement.bind(this);
  //   }

  render() {
    return (
      <div>
        <span className={this.getClassColor()}>{this.formatCount()}</span>
        <button
          onClick={() => this.handleIncrement(this.state.product)}
          className="btn btn-secondary btn-sm m-2">
          CHECK
        </button>
        <button onClick={this.resetCount} className="btn btn-secondary btn-sm">
          RESET
        </button>
      </div>
    );
  }

  handleIncrement = (product) => {
    console.log("Clicked");
    this.setState({ count: this.state.count + 1 });
  };

  resetCount = () => {
    this.setState({ count: this.state.count * 0 });
  };

  renderTags() {
    if (this.state.tags.length === 0) return <p>Please create tags</p>;
    return (
      <ul>
        {this.state.tags.map((tag) => (
          <li key={tag}>{tag}</li>
        ))}
      </ul>
    );
  }

  getClassColor() {
    let classes = "badge m-2 badge-";
    classes += this.state.count === 0 ? "warning" : "primary";
    return classes;
  }

  junk() {
    return "Junks";
  }

  formatCount() {
    const { count } = this.state;
    return count === 0 ? "0" : count;
  }
}
